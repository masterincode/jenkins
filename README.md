create table test_employee(
 emp_id int primary key,
 emp_name varchar2(20),
 emp_city varchar2(20),
 manager_id number(10),
 department_id integer
 );
 
 insert into test_employee(emp_id,emp_name,emp_address,manager_id,department_id) values (100,'Dhoni','Ranchi',null,1);
 insert into test_employee(emp_id,emp_name,emp_address,manager_id,department_id) values (101,'Virat','Delhi',null,2);
 insert into test_employee(emp_id,emp_name,emp_address,manager_id,department_id) values (102,'Rohit','Mumbai',null,3);
 insert into test_employee(emp_id,emp_name,emp_address,manager_id,department_id) values (103,'Pant','Mumbai',100,1);
 insert into test_employee(emp_id,emp_name,emp_address,manager_id,department_id) values (104,'Dhawan','Delhi',101,2);
 insert into test_employee(emp_id,emp_name,emp_address,manager_id,department_id) values (105,'Bhumi','Mumbai',102,3);
 insert into test_employee(emp_id,emp_name,emp_address,manager_id,department_id) values (106,'Ajinkya','Kolkatta',100,1);
 insert into test_employee(emp_id,emp_name,emp_address,manager_id,department_id) values (107,'Umesh','Delhi',102,2);
 insert into test_employee(emp_id,emp_name,emp_address,manager_id,department_id) values (108,'Ambati','Mumbai',102,3);
 insert into test_employee(emp_id,emp_name,emp_address,manager_id,department_id) values (109,'Mayank','Ranchi',101,1);
 insert into test_employee(emp_id,emp_name,emp_address,manager_id,department_id) values (110,'Raina','Delhi',100,2);
 insert into test_employee(emp_id,emp_name,emp_address,manager_id,department_id) values (112,'Jadeja','Mumbai',100,3);
 
 
select a.emp_name,a.emp_city ,b.EMP_NAME from  test_employee a join test_employee b
on A.MANAGER_ID=b.EMP_ID;

create table test_department(

 DEPARTMENT_ID INTEGER primary key,
 dept_name varchar2(20),
 dept_faculty_size int,
 location_id int
);


insert into test_department(DEPARTMENT_ID,DEPT_NAME,dept_faculty_size,location_id) values (2,'HR',10,20);
insert into test_department(DEPARTMENT_ID,DEPT_NAME,dept_faculty_size,location_id) values (3,'Finance',20,30);
insert into test_department(DEPARTMENT_ID,DEPT_NAME,dept_faculty_size,location_id) values (4,'Opeartion',15,40);
insert into test_department(DEPARTMENT_ID,DEPT_NAME,dept_faculty_size,location_id) values (5,'IT',10,50);
insert into test_department(DEPARTMENT_ID,DEPT_NAME,dept_faculty_size,location_id) values (6,'BA',40,60);


